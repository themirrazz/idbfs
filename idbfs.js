window.IdbFS=(function () {
    var Idb=function(partition,cb) {
        var idb=(window.indexedDB||window.webkitIndexedDB||window.mozIndexedDB||window.msIndexedDB);
        var db;
        var request=idb.open("IdbFS:"+partition,1);
        request.onsuccess=e=>{
            db=e.target.result
            cb()
        }
        request.onerror=()=>{console.error("IndexedDB error!")};
        request.onupgradeneeded=e=>{
            var tgt=e.target.result;
            var store=tgt.createObjectStore("FS",{
                keyPath:'path'
            });
            store.transaction.oncomplete=o=>{
                db=o.target.db;
                cb()
            }
        }
        return {
            encodeKey:(key)=>{
                var out=[];
                for(var i=0;i<key.length;i++){
                    out.push(key.charCodeAt(i))
                }
                return out.join(":");
            },
            decodeKey:(cryp)=>{
                var out="";
                var enc=cryp.split(":");
                for(var i=0;i<enc.length;i++){
                    out+=String.fromCharCode(enc[i])
                }
                return out
            },
            get: function (key) {
                key=this.encodeKey(key);
                return new Promise((resolve,reject)=>{
                    var g=db.transaction("FS").objectStore("FS").get(key);
                    g.onsuccess=event=>{
                    if(event.target.result) {
                        resolve(event.target.result.data||null)
                    }else{resolve(null)}
                    }
                    g.onerror=()=>reject(new Error("IDB error"))
                });
            },
            set: function (key,value) {
                key=this.encodeKey(key);
                return new Promise((resolve,reject)=>{
                    var g=db.transaction("FS",'readwrite').objectStore("FS").put({
                    path:key,data:value
                    });
                    g.onsuccess=()=>{
                    resolve()
                    }
                    g.onerror=()=>reject(new Error("IDB error"))
                });
            },
            rm: function (key) {
                key=this.encodeKey(key);
                return new Promise((resolve,reject)=>{
                    var g=db.transaction("FS",'readwrite').objectStore("FS").delete(key);
                    g.onsuccess=()=>{
                    resolve()
                    }
                    g.onerror=()=>reject(new Error("IDB error"))
                });
            },
            keys:function(){
                return new Promise((resolve,reject)=>{
                    var g=db.transaction("FS").objectStore("FS").getAllKeys();
                    g.onsuccess=()=>{
                    var a=[];
                    g.result.forEach(b=>{
                        a.push(this.decodeKey(b))
                    })
                    resolve(a)
                    }
                    g.onerror=()=>reject(new Error("IDB error"))
                })
            }
        }
    };
    var FS=(class IndexedDBFileSystem {
        constructor(partition) {
            if(!partition) {
                throw new TypeError("a partition name must be specified")
            }
            this.#db=new Idb(partition,()=>this.#init());
            this.#mounts={};
        }
        #db
        #mounts
        #getMountsWithPrefix(prefix) {
            var e=[];
            for(var i=0;i<Object.keys(this.#mounts).length;i++) {
                if(Object.keys(this.#mounts)[i].startsWith(prefix)) {e.push(this.#mounts[Object.keys(this.#mounts)[i]])}
            }
            return e
        }
        async mount(disk,path) {
            path=this.#correctPath(path)
            if(!await this.#parentDirExists(path)) {
                throw new Error('no such file or directory')
            }
            if(await this.#exists(path)) {
                throw new Error('mount location already taken')
            }
            this.#mounts[path]=disk
        }
        async umount(path) {
            path=this.#correctPath(path)
            if(!this.#mounts[path]) {
                throw new Error('no disk mounted at path')
            }
            this.#mounts[path]=undefined;
        }
        async #init() {
            if(!await this.exists("/")) {
                var date=new Date();
                await this.#db.set("/", {
                    creationDate: date.toString(),
                    modificationDate: date.toString(),
                    accessedDate: date.toString(),
                    filetype: 0,
                    properties: [],
                    allowExecuting: true
                });
            }
        }
        #parentDirExists(path) {
            path=this.#correctPath(path);
            if(path==="/") { return true }
            return (async ()=>{
            var spl=path.split("/");
            for(var i=0;i<spl.length-1;i++) {
                if(await this.#exists(this.#correctPath(
                spl.slice(0,i+1).join("/")
                ))) {
                if(await this.#isFile(this.#correctPath(
                    spl.slice(0,i+1).join("/")
                ))) {
                    return false
                }
                } else {
                return false
                }
            }
            return true
            })();
        }
        #getMount(path) {
            path=this.#correctPath(path);
            if(path==="/") { return true }
            return (()=>{
            var spl=path.split("/");
            for(var i=0;i<spl.length-1;i++) {
                if(this.#mounts[spl.slice(0,i+1).join("//")]) {
                    return this.#mounts[
                        spl.slice(0,i+1).join("//")
                    ]
                }
            }
            return null
            })();
        }
        async #exists(path) {
            path=this.#correctPath(path);
            if(await this.#db.get(path)) {
                return true;
            }
            return false;
        }
        async exists(path) {
            path=this.#correctPath(path);
            if(this.#getMount(path)) {
               return await (this.#getMount(path)).exists((path.slice(this.#getMount(path).length))); 
            }
            if(!await this.#parentDirExists(path)) {
                return false
            }
            if(await this.#db.get(path)) {
                return true;
            }
            return false;
        }
        async #isFile(path) {
            path=this.#correctPath(path);
            if(!await this.#exists(path)) {
                throw new Error("no such file or directory")
            }
            if((await this.#db.get(path)).filetype===1) { return true }
            return false;
        }
        async isFile(path) {
            path=this.#correctPath(path);
            if(this.#getMount(path)) {
               return await (this.#getMount(path)).isFile((path.slice(this.#getMount(path).length))); 
            }
            if(!await this.exists(path)) {
            throw new Error("no such file or directory")
            }
            if((await this.#db.get(path)).filetype===1) { return true }
            return false;
        }
        async mkdir(path) {
            path=this.#correctPath(path);
            if(this.#getMount(path)) {
               return await (this.#getMount(path)).mkdir((path.slice(this.#getMount(path).length))); 
            }
            var date=new Date();
            if(!await this.#parentDirExists(path)) {
                throw new Error("no such file or directory");
            }
            if(await this.#exists(path)) { return }
            await this.#db.set(path, {
                creationDate: date.toString(),
                modificationDate: date.toString(),
                accessedDate: date.toString(),
                filetype: 0,
                properties: [],
                allowExecuting: true
            });
        }
        async touch(path) {
            path=this.#correctPath(path);
            if(this.#getMount(path)) {
               return await (this.#getMount(path)).touch((path.slice(this.#getMount(path).length))); 
            }
            var date=new Date();
            if(!await this.#parentDirExists(path)) {
                throw new Error("no such file or directory");
            }
            if(await this.#exists(path)) { return }
            await this.#db.set(path, {
                creationDate: date.toString(),
                modificationDate: date.toString(),
                accessedDate: date.toString(),
                filetype: 1,
                properties: [],
                allowExecuting: true,
                content: []
            });
        }
        async stat(path) {
            path=this.#correctPath(path);
            if(this.#getMount(path)) {
               return await (this.#getMount(path)).stat((path.slice(this.#getMount(path).length))); 
            }
            if(!await this.exists(path)) {
                throw new Error("no such file or directory")
            }
            var stats=await this.#db.get(path);
            return {
                creationDate: stats.creationDate,
                modificationDate: stats.modificationDate,
                accessedDate: stats.accessedDate,
                filetype: stats.filetype,
                allowExecuting: stats.allowExecuting
            }
        }
        async getprops(path) {
            path=this.#correctPath(path);
            if(this.#getMount(path)) {
               return await (this.#getMount(path)).getprops((path.slice(this.#getMount(path).length))); 
            }
            if(!await this.exists(path)) {
                throw new Error("no such file or directory")
            }
            var stats=await this.#db.get(path);
            return stats.properties;
        }
        async rmprop(path,prop) {
            path=this.#correctPath(path);
            if(this.#getMount(path)) {
               return await (this.#getMount(path)).rmprop(path,prop); 
            }
            if(!await this.exists(path)) {
                throw new Error("no such file or directory")
            }
            var stats=await this.#db.get(path);
            var props=stats.properties;
            if(!props.includes(prop)) {
                throw new Error("property does not exist on entity")
            }
            var d=[];
            for(var i=0;i<props.length;i++) {
                if(props[i]!==String(prop)) {
                    d.push(props[i])
                }
            }
            stat.properties=d;
            stat.modificationDate=(new Date()).toString();
            await this.#db.set(path,stat);
        }
        async addprop(path,prop) {
            path=this.#correctPath(path);
            if(this.#getMount(path)) {
               return await (this.#getMount(path)).addprop(path,prop); 
            }
            if(!await this.exists(path)) {
                throw new Error("no such file or directory")
            }
            var stats=await this.#db.get(path);
            var props=stats.properties;
            if(props.includes(prop)) {
                throw new Error("property already exists on entity")
            }
            var d=[];
            d.push(prop);
            stat.properties=d;
            stat.modificationDate=(new Date()).toString();
            await this.#db.set(path,stat);
        }
        async hasprop(path,prop) {
            path=this.#correctPath(path);
            if(this.#getMount(path)) {
               return await (this.#getMount(path)).hasprop(path,prop); 
            }
            if(!await this.exists(path)) {
                throw new Error("no such file or directory")
            }
            var stats=await this.#db.get(path);
            var props=stats.properties;
            if(props.includes(prop)) {
                return true
            }
            return false
        }
        async readstr(path) {
            var bin=await this.readbin(path);
            var str="";
            for(var i=0;i<bin.length;i++) {
                str+=String.fromCharCode(bin[i])
            }
            return str;
        }
        async readbin(path) {
            path=this.#correctPath(path);
            if(this.#getMount(path)) {
               return await (this.#getMount(path)).readbin((path.slice(this.#getMount(path).length))); 
            }
            if(!await this.exists(path)) {
                throw new Error("no such file or directory");
            }
            if(!await this.isFile(path)) {
                throw new Error("entity is a directory")
            }
            this.#db.get(path).then(data=>{
                data.accessedDate=(new Date()).toString();
                this.#db.set(path,data)
            })
            return (await this.#db.get(path)).content;
        }
        async writestr(path,str) {
            var bin=[];
            for(var i=0;i<str.length;i++) {
                bin.push(str.charCodeAt(i));
            }
            await this.writebin(path,bin);
        }
        async rm(path) {
            path=this.#correctPath(path);
            if(this.#getMount(path)) {
               return await (this.#getMount(path)).rm((path.slice(this.#getMount(path).length))); 
            }
            if(!await this.isFile(path)) {
                var walk=this.readdir(path);
                for(var i=0;i<walk.length;i++) {
                    await this.#db.rm(walk[i])
                }
                this.#getMountsWithPrefix(path).forEach(e=>{
                    this.umount(e);
                })
            }
            return await Idb.rm(path)
        }
        async walk(path) {
            path=this.#correctPath(path);
            if(this.#getMount(path)) {
               return await (this.#getMount(path)).walk((path.slice(this.#getMount(path).length))); 
            }
            if(!await this.exists(path)) {
                throw new Error("no such file or directory");
            }
            if(await this.isFile(path)) {
                throw new Error("entity is a file")
            }
            this.#db.get(path).then(data=>{
                data.accessedDate=(new Date()).toString();
                this.#db.set(path,data);
            });
            var keys=await this.#db.keys();
            if(path==="/") { path="" }
            var e=[]
            for(var i=0;i<keys.length;i++) {
                if(keys[i].startsWith(path+"/")) {
                    e.push(keys[i])
                }
            }
            return e
        }
        async readdir(path) {
            path=this.#correctPath(path);
            if(path==="/") { path="" }
            var walk=await this.walk(path)
            var e=[]
            for(var i=0;i<walk.length;i++) {
                if(path.split("/").length+1===walk[i].split("/").length) {
                    e.push(walk[i])
                }
            }
            return e
        }
        async writebin(path,bin) {
            path=this.#correctPath(path);
            if(this.#getMount(path)) {
               return await (this.#getMount(path)).writebin((path.slice(this.#getMount(path).length)),bin); 
            }
            if(!await this.exists(path)) {
                await this.touch(path);
            }
            if(!await this.isFile(path)) {
                throw new Error("entity is a directory")
            }
            var data=await this.#db.get(path);
            data.content=this.#ui8toarr(bin);
            data.modificationDate=(new Date()).toString();
            await this.#db.set(path,data);
        }
        #ui8toarr(ui8) {
            var arr=[];
            for(var i=0;i<ui8.length;i++) {
                arr.push(ui8[i])
            }
            return arr;
        }
        #correctPath(path) {
            if(path==="/"||path==="") {
                return "/"
            }
            if(!path.startsWith("/")) {
                path==="/"+path;
            }
            var gO="";
            var wS=false
            for(var i=0;i<path.length;i++) {
                if(path[i]==="/") {
                    if(!wS) { gO+="/" }
                    wS=true;
                } else {
                    wS=false
                    gO+=path[i];
                }
            }
            if(gO==="/") {
                return "/"
            }
            while(gO.endsWith("/")) {
                gO=gO.slice(0,gO.length-1);
            }
            var tW=gO.split("/");
            var dG=[];
            for(var i=0;i<tW.length;i++) {
                if(tW[i]==="..") {
                    if(dG.length>1) {
                        dG.pop();
                    }
                } else if(tW[i]!==".") {
                    dG.push(tW[i])
                }
            }
            return tW.join("/")
        }
    });
    return FS;
})();
